$( document ).ready(function() {

    var flight ;

    $('#postCustomersBtn').click(function(){

        ajaxPost();
    });


    function validateForm() {
        var a = document.forms["form"]["whereFrom"].value;
        var b = document.forms["form"]["whereTo"].value;
        var c = document.forms["form"]["dateAndTimeDeparture"].value;
        var d = document.forms["form"]["dateAndTimeArrival"].value;
        var e = document.forms["form"]["places"].value;
        var f = document.forms["form"]["price"].value;

        if( a=="" || b == "" || c == "" || d =="" || e =="" || f==""){
            alert("Fill all required fields!");
            return false;
        }

        return true;

    }

    function ajaxPost(){
//                event.preventDefault();


        if(validateForm()) {

            var formCustomer = {
                whereFrom: $("#whereFrom").val(),
                whereTo: $("#whereTo").val(),
                dateAndTimeDeparture: $("#dateAndTimeDeparture").val(),
                dateAndTimeArrival: $("#dateAndTimeArrival").val(),
                places: $("#places").val(),
                price: $("#price").val()

            };
            flight = formCustomer;


//                alert(flight.name);
            // DO POST
            $.ajax({
                type: "POST",
                contentType: "application/json",
                accept: 'text/plain',
                url: "/flight/send",
                data: JSON.stringify(flight),
                dataType: 'text',
                success: function (result) {
                    // clear customer body

                    // re-set customer table list

                    // fill successfully message on view
                    $("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" +
                        result +
                        "</p>");
                },
//                    error : function(e) {
//                        alert("Error!")
//                        console.log("ERROR: ", e);
//                    }

            });
            setTimeout(window.location.replace("/flight/list"), 3000);
        }

    }





})