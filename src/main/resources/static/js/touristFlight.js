$( document ).ready(function() {


    ajaxGet();

    function ajaxGet() {

        var xmlhttp = new XMLHttpRequest();
        var url = window.location.pathname+"/add";

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);

                fillTable(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    function fillTable(result) {



        $('#flights').bootstrapTable({
            data: result


        });


        for(var row_index =0;row_index<result.length;row_index++  )
        {

            $('#flights').bootstrapTable('updateRow', {index: row_index, row: {
                add:"<a href='"+window.location.pathname+"/"+result[row_index].id+"/add'  class=\"btn btn-success\"><i class=\"glyphicon glyphicon-plus\"></i></a>"
            }});
        }

        $('#flights').bootstrapTable('hideLoading');

    }
})