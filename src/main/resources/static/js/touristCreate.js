$( document ).ready(function() {

    var tourist ;

    $('#postCustomersBtn').click(function(){

        ajaxPost();
    });


    function validateForm() {
        var a = document.forms["form"]["name"].value;
        var b = document.forms["form"]["surname"].value;
        var c = document.forms["form"]["country"].value;
        var d = document.forms["form"]["sex"].value;
        var e = document.forms["form"]["dateOfBirth"].value;

        if( a=="" || b == "" || c == "" || d =="" || e ==""){
            alert("Fill all required fields!");
            return false;
        }

        return true;

    }

    function ajaxPost(){
//                event.preventDefault();


        if(validateForm()) {

            var formCustomer = {
                name: $("#name").val(),
                surname: $("#surname").val(),
                country: $("#country").val(),
                sex: $("#sex").val(),
                dateOfBirth: $("#dateOfBirth").val()

            };
            tourist = formCustomer;

            $.ajax({
                type: "POST",
                contentType: "application/json",
                accept: 'text/plain',
                url: "/tourist/send",
                data: JSON.stringify(tourist),
                dataType: 'text',
                success: function (result) {
                    $("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" +
                        result +
                        "</p>");
                },

            });
            setTimeout(window.location.replace("/tourist/list"), 3000);
        }

    }

})