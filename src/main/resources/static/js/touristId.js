$(document).ready(function() {

    ajaxGet();

    var id;

    function setText(id,newvalue) {
        var s= document.getElementById(id);
        s.innerHTML = newvalue;

    }
    window.onload=function() {
        setText("birth","Hello there");
    }
    function ajaxGet() {

        var xmlhttp = new XMLHttpRequest();
        var url = window.location.pathname+"/info";

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);

                fillTable(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    function fillTable(result) {


        $('#table').bootstrapTable({
            data: result


        });

        var temp = result[0].flights;
        id=result[0].id;


        $('#flights').bootstrapTable({
            data: temp


        });


        for(var row_index =0;row_index<temp.length;row_index++  )
        {

            var flightId = temp[row_index].id;
            $('#flights').bootstrapTable('updateRow', {index: row_index, row: {
                action:"<a href='/tourist/"+id+"/flight/"+flightId+"/delete'  class=\"btn btn-danger\">Delete <i class=\"glyphicon glyphicon-remove\"></i></a>"
            }});
        }

        $('#table').bootstrapTable('hideLoading');
        $('#flights').bootstrapTable('hideLoading');



    }

    $('#deleteTouristBtn').click(function(){
        removeTourist();
    });

    function removeTourist() {

//                alert(id);
        $.ajax({
            type : "DELETE",
            contentType : "application/json",
            accept: 'text/plain',
            url : "/tourist/delete/"+id,
            data : id,//JSON.stringify(tourist),
            dataType: 'text'


        });

        setTimeout( window.location.replace("/tourist/list"),3000);

    }

    $('#addFlightBtn').click(function(){
        addFlight();
    });

    function addFlight() {

        window.location.replace("/tourist/"+id+"/flight");

    }

})