$( document ).ready(function() {

    ajaxGet();

    // DO GET
    function ajaxGet() {

        var xmlhttp = new XMLHttpRequest();
        var url = "/tourist/list/all";

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);


                for(var i=0; i<myArr.length;i++){
                }
                fillTable(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
})

function fillTable(result) {



    $('#table').bootstrapTable({
        data: result


    });

    $('#table').on('click-row.bs.table', function (row, arg1, arg2) {

        processRow(arg1);

    });

    for(var row_index =0;row_index<result.length;row_index++  )
    {

        $('#table').bootstrapTable('updateRow', {index: row_index, row: {
            info:"<a href='/tourist/"+result[row_index].id+"'  class=\"btn btn-info\">Info <i class=\"glyphicon glyphicon-plane\"></i></a>"
        }});
    }

    $('#table').bootstrapTable('hideLoading');



}
function processRow(data) {
    window.location.replace("/tourist/"+data.id);
}