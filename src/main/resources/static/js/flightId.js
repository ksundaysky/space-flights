$(document).ready(function() {

    ajaxGet();

    var res;

    var id;

    function ajaxGet() {

        var xmlhttp = new XMLHttpRequest();
        var url = window.location.pathname+"/info";

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                res = myArr;

                fillTable(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    function fillTable(result) {


        $('#flights').bootstrapTable({
            data: result


        });

        var temp = result[0].tourists;
        id=result[0].id;


        $('#table').bootstrapTable({
            data: temp


        });


        for(var row_index =0;row_index<temp.length;row_index++  )
        {

            var touristId = temp[row_index].id;
            $('#table').bootstrapTable('updateRow', {index: row_index, row: {
                action:"<a href='/flight/"+id+"/tourist/"+touristId+"/delete'  class=\"btn btn-danger\">Delete <i class=\"glyphicon glyphicon-remove\"></i></a>"
            }});
        }

        $('#table').bootstrapTable('hideLoading');
        $('#flights').bootstrapTable('hideLoading');



    }

    $('#deleteTouristBtn').click(function(){
        removeFlight();
    });

    function removeFlight() {

//                alert(id);
        $.ajax({
            type : "DELETE",
            contentType : "application/json",
            accept: 'text/plain',
            url : "/flight/delete/"+id,
            data : id,//JSON.stringify(tourist),
            dataType: 'text'


        });

        setTimeout( window.location.replace("/flight/list"),3000);

    }

    $('#addFlightBtn').click(function(){
        addFlight();
    });

    function addFlight() {

        if(res[0].places <= 0)
        {
            alert("This flight is full!!!")
        }
        else {
            window.location.replace("/flight/" + id + "/tourist");
        }
    }

})
