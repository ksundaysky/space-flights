package com.ksundaysky.spaceflights.converters;

import com.ksundaysky.spaceflights.commands.GetFlight;
import com.ksundaysky.spaceflights.commands.TouristCommand;
import com.ksundaysky.spaceflights.domain.Flight;
import com.ksundaysky.spaceflights.domain.Tourist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

import java.text.SimpleDateFormat;

public class TouristToTouristCommand implements Converter<Tourist,TouristCommand> {
    @Nullable
    @Override
    public TouristCommand convert(Tourist tourist) {

        TouristCommand touristCommand = new TouristCommand();
        touristCommand.setId(tourist.getId());
        touristCommand.setCountry(tourist.getCountry());
        touristCommand.setName(tourist.getName());
        touristCommand.setSurname(tourist.getSurname());
        touristCommand.setSex(tourist.getSex());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        touristCommand.setDateOfBirth(sdf.format(tourist.getDateOfBirth()));
        for(Flight f : tourist.getFlights()) {

            touristCommand.getFlights().add(new GetFlight(f));
        }
        return touristCommand;
    }
}
