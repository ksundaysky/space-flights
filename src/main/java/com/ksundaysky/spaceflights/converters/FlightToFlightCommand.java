package com.ksundaysky.spaceflights.converters;

import com.ksundaysky.spaceflights.commands.FlightCommand;
import com.ksundaysky.spaceflights.commands.GetTourist;
import com.ksundaysky.spaceflights.domain.Flight;
import com.ksundaysky.spaceflights.domain.Tourist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

import java.text.SimpleDateFormat;

public class FlightToFlightCommand implements Converter<Flight,FlightCommand>{
    @Nullable
    @Override
    public FlightCommand convert(Flight flight) {
        SimpleDateFormat sdft = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        FlightCommand flightCommand = new FlightCommand();
        flightCommand.setDateAndTimeArrival(sdft.format(flight.getDateAndTimeArrival()));
        flightCommand.setDateAndTimeDeparture(sdft.format(flight.getDateAndTimeDeparture()));
        flightCommand.setId(flight.getId());
        flightCommand.setPlaces(flight.getPlaces());
        flightCommand.setWhereFrom(flight.getWhereFrom());
        flightCommand.setWhereTo(flight.getWhereTo());
        flightCommand.setPrice(flight.getPrice());

        for(Tourist t : flight.getTourists()){
            flightCommand.getTourists().add(new GetTourist(t));
        }

        return flightCommand;
    }
}
