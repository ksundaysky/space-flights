package com.ksundaysky.spaceflights.converters;

import com.ksundaysky.spaceflights.commands.FlightCommand;

import com.ksundaysky.spaceflights.domain.Flight;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FlightCommandToFlight implements Converter<FlightCommand,Flight> {


    @Nullable
    @Override
    public Flight convert(FlightCommand flightCommand) {
        Flight flight = new Flight();
        SimpleDateFormat sdft = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
        flight.setDateAndTimeArrival(sdft.parse(flightCommand.getDateAndTimeArrival()));

            flight.setDateAndTimeDeparture(sdft.parse(flightCommand.getDateAndTimeDeparture()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        flight.setId(flightCommand.getId());
        flight.setPlaces(flightCommand.getPlaces());
        flight.setWhereFrom(flightCommand.getWhereFrom());
        flight.setWhereTo(flightCommand.getWhereTo());
        flight.setPrice(flightCommand.getPrice());

        return flight;
    }
}

