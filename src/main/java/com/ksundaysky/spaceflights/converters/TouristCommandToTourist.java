package com.ksundaysky.spaceflights.converters;

import com.ksundaysky.spaceflights.commands.GetFlight;
import com.ksundaysky.spaceflights.commands.TouristCommand;
import com.ksundaysky.spaceflights.domain.Flight;
import com.ksundaysky.spaceflights.domain.Tourist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TouristCommandToTourist implements Converter<TouristCommand,Tourist> {

    @Nullable
    @Override
    public Tourist convert(TouristCommand touristCommand) {

        Tourist tourist = new Tourist();
//        touristCommand.setId(tourist.getId());
        tourist.setCountry(touristCommand.getCountry());
        tourist.setName(touristCommand.getName());
        tourist.setSurname(touristCommand.getSurname());
        tourist.setSex(touristCommand.getSex());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = sdf.parse(touristCommand.getDateOfBirth());
            tourist.setDateOfBirth(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tourist;
    }
}
