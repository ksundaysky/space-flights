package com.ksundaysky.spaceflights.controllers;

import com.ksundaysky.spaceflights.commands.FlightCommand;
import com.ksundaysky.spaceflights.commands.GetFlight;
import com.ksundaysky.spaceflights.commands.GetTourist;
import com.ksundaysky.spaceflights.commands.TouristCommand;
import com.ksundaysky.spaceflights.converters.FlightCommandToFlight;
import com.ksundaysky.spaceflights.converters.FlightToFlightCommand;
import com.ksundaysky.spaceflights.converters.TouristCommandToTourist;
import com.ksundaysky.spaceflights.converters.TouristToTouristCommand;
import com.ksundaysky.spaceflights.domain.Flight;
import com.ksundaysky.spaceflights.domain.Tourist;
import com.ksundaysky.spaceflights.repositories.FlightRepository;
import com.ksundaysky.spaceflights.repositories.TouristRepository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@org.springframework.web.bind.annotation.RestController

public class RestController
{
   private TouristRepository touristRepository;
   private FlightRepository flightRepository;
   private TouristToTouristCommand converter;
   private TouristCommandToTourist converterTourist;
   private FlightToFlightCommand converterFlight;
   private FlightCommandToFlight converterFlightCommandToFlight;

    public RestController(TouristRepository touristRepository, FlightRepository flightRepository) {
        this.touristRepository = touristRepository;
        this.flightRepository = flightRepository;
        this.converter = new TouristToTouristCommand();
        this.converterTourist = new TouristCommandToTourist();
        this.converterFlight = new FlightToFlightCommand();
        this.converterFlightCommandToFlight = new FlightCommandToFlight();
    }

    @GetMapping("/tourist/list/all")
    public List<Tourist> touristList(){
        List<Tourist> tourists = new ArrayList<>();
        touristRepository.findAll().iterator().forEachRemaining(tourists::add);
        return tourists ;
    }

    @GetMapping("/tourist/{id}/info")
    public List<TouristCommand> touristInfo(@PathVariable Long id){
        System.out.println(id);

        TouristCommand touristCommand = converter.convert(touristRepository.findById(id).get());
        for (GetFlight f:touristCommand.getFlights()) {
            System.out.println(f.getWhereFrom());

        }
        return Arrays.asList(touristCommand);
    }

    @GetMapping("/flight/{id}/info")
    public List<FlightCommand> flightInfo(@PathVariable Long id){
        System.out.println(id);

        FlightCommand flightCommand = converterFlight.convert(flightRepository.findById(id).get());

        return Arrays.asList(flightCommand);
    }

    @PostMapping("/tourist/send")
    public String postTourist(@RequestBody TouristCommand  touristCommand){

        System.out.println(touristCommand.getName());
        System.out.println("date ===== "+touristCommand.getDateOfBirth());
        System.out.println("sex ===== "+touristCommand.getSex());

        touristRepository.save(converterTourist.convert(touristCommand));
        return "Post Successfully";
    }

    @PostMapping("/flight/send")
    public String postFlight(@RequestBody FlightCommand  flightCommand){

        flightRepository.save(converterFlightCommandToFlight.convert(flightCommand));
        return "Post Successfully";
    }

    @DeleteMapping("/tourist/delete/{id}")
    public  String deleteTourist(@PathVariable Long id){

        touristRepository.deleteById(id);
        return "Success!";
    }

    @GetMapping("/tourist/{id}/flight/add")
    public List<GetFlight> flightsToChoose(@PathVariable Long id){
        List<Flight> flights = touristRepository.findById(id).get().getFlights();

        List<GetFlight> getFlights = new ArrayList<>();
        List<Flight> flightsRemaining = new ArrayList<>();
        flightRepository.findAll().iterator().forEachRemaining(flight -> flightsRemaining.add(flight));

        for (Flight f:flights) {


            if(flightsRemaining.contains(f) )
                flightsRemaining.remove(f);
        }


        for(Iterator<Flight> it = flightsRemaining.iterator(); it.hasNext();){

            Flight f = it.next();

            if(f.getPlaces()<=0)
            {
                it.remove();
            }
            else
                getFlights.add(new GetFlight(f));

        }



        return getFlights;
    }

    @GetMapping("/flight/list/all")
    public List<Flight> getListFlights(){

        List<Flight> flightCommands = new ArrayList<>();
        flightRepository.findAll().iterator().forEachRemaining(flightCommands::add);
        return flightCommands;
    }

    @DeleteMapping("/flight/delete/{id}")
    public String deleteFlight(@PathVariable Long id){

        flightRepository.deleteById(id);
        return "Success";
    }

    @GetMapping("/flight/{id}/tourist/add")
    public List<GetTourist> touristsToChoose(@PathVariable Long id){
        List<Tourist> tourists = flightRepository.findById(id).get().getTourists();

        List<GetTourist> getTourists = new ArrayList<>();
        List<Tourist> touristRemaining = new ArrayList<>();
        touristRepository.findAll().iterator().forEachRemaining(flight -> touristRemaining.add(flight));

        for (Tourist t:tourists) {

            if(touristRemaining.contains(t))
                touristRemaining.remove(t);
        }

        for (Tourist t:touristRemaining) {

            getTourists.add(new GetTourist(t));
        }
        return getTourists;
    }

}

