package com.ksundaysky.spaceflights.controllers;

import com.ksundaysky.spaceflights.domain.Flight;
import com.ksundaysky.spaceflights.domain.Tourist;
import com.ksundaysky.spaceflights.repositories.FlightRepository;
import com.ksundaysky.spaceflights.repositories.TouristRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
public class IndexController {

    private TouristRepository touristRepository;
    private FlightRepository flightRepository;

    public IndexController(TouristRepository touristRepository, FlightRepository flightRepository) {
        this.touristRepository = touristRepository;
        this.flightRepository = flightRepository;
    }

    @RequestMapping({"/","/home"})
    public String index(){
        return "index";
    }

    @RequestMapping("/tourist/list")
    public String touristList(){
        return "/tourist/list";
    }

    @RequestMapping("/tourist/create")
    public String touristCreate(){
        return "/tourist/create";
    }

    @RequestMapping("/flight/create")
    public String flightCreate(){
        return "/flight/create";
    }

    @RequestMapping("/flight/list")
    public String flightList(){
        return "/flight/list";
    }

    @RequestMapping("/tourist/{id}")
    public String touristById(){
        return "/tourist/id";
    }

    @RequestMapping("/flight/{id}")
    public String flightById(){
        return "/flight/id";
    }


    @RequestMapping("/tourist/{id}/flight")
    public  String getTouristFlights(){
        return "/tourist/flight";
    }

    @RequestMapping("/flight/{id}/tourist")
    public  String getFlightTourists(){
        return "/flight/tourist";
    }

    @RequestMapping("tourist/{touristId}/flight/{flightId}/delete")
    public String deletFlightFromTourist(@PathVariable Long touristId, @PathVariable Long flightId){

        Tourist tourist = touristRepository.findById(touristId).get();
        List<Flight> flights = tourist.getFlights();

        for(Iterator<Flight> it = flights.iterator();it.hasNext();){

            Flight f = it.next();
            if(f.getId().equals(flightId))
                it.remove();
                f.setPlaces(f.getPlaces()+1);

        }

        tourist.setFlights(flights);
        touristRepository.save(tourist);
        return "redirect:/tourist/"+touristId;
    }

    @RequestMapping("flight/{flightId}/tourist/{touristId}/delete")
    public String deleteTouristFromFlight(@PathVariable Long touristId, @PathVariable Long flightId){

        deletFlightFromTourist(touristId,flightId);
        return "redirect:/flight/"+flightId;
    }

    @RequestMapping("tourist/{touristId}/flight/{flightId}/add")
    public String addFlightToTourist(@PathVariable Long touristId, @PathVariable Long flightId){

        Tourist tourist = touristRepository.findById(touristId).get();
        Flight f =flightRepository.findById(flightId).get();
        tourist.getFlights().add(f);
        f.setPlaces(f.getPlaces()-1);
        touristRepository.save(tourist);
        flightRepository.save(f);
        return "redirect:/tourist/"+touristId;
    }

    @RequestMapping("flight/{flightId}/tourist/{touristId}/add")
    public String addTouristToFlight(@PathVariable Long touristId, @PathVariable Long flightId){

        Tourist tourist = touristRepository.findById(touristId).get();
        Flight flight = flightRepository.findById(flightId).get();
        tourist.getFlights().add(flight);
        flight.setPlaces(flight.getPlaces()-1);
        touristRepository.save(tourist);
        return "redirect:/flight/"+flightId;
    }


}
