package com.ksundaysky.spaceflights.commands;

import com.ksundaysky.spaceflights.domain.Tourist;
import lombok.Data;

import java.text.SimpleDateFormat;

@Data
public class GetTourist {

    private Long id;

    private String name;
    private String surname;
    private String sex;
    private String country;
    private String dateOfBirth;

    public GetTourist(Tourist tourist) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

           this.dateOfBirth = sdf.format(tourist.getDateOfBirth());

        this.id = tourist.getId();
        this.name =tourist.getName();
        this.surname = tourist.getSurname();
        this.sex = tourist.getSex();
        this.country = tourist.getCountry();
    }
}
