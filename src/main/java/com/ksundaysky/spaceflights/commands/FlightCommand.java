package com.ksundaysky.spaceflights.commands;

import java.util.ArrayList;
import java.util.List;

public class FlightCommand {

    private Long id;

    private String dateAndTimeDeparture;
    private String dateAndTimeArrival;
    private  String whereFrom;
    private  String whereTo;
    private Long places;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    private Double price;

    private List<GetTourist> tourists = new ArrayList<>();

    public List<GetTourist> getTourists() {
        return tourists;
    }

    public void setTourists(List<GetTourist> tourists) {
        this.tourists = tourists;
    }

    public Long getId() {
        return id;

    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDateAndTimeDeparture() {
        return dateAndTimeDeparture;
    }

    public void setDateAndTimeDeparture(String dateAndTimeDeparture) {
        this.dateAndTimeDeparture = dateAndTimeDeparture;
    }

    public String getDateAndTimeArrival() {
        return dateAndTimeArrival;
    }

    public void setDateAndTimeArrival(String dateAndTimeArrival) {
        this.dateAndTimeArrival = dateAndTimeArrival;
    }

    public String getWhereFrom() {
        return whereFrom;
    }

    public void setWhereFrom(String whereFrom) {
        this.whereFrom = whereFrom;
    }

    public String getWhereTo() {
        return whereTo;
    }

    public void setWhereTo(String whereTo) {
        this.whereTo = whereTo;
    }

    public Long getPlaces() {
        return places;
    }

    public void setPlaces(Long places) {
        this.places = places;
    }


}
