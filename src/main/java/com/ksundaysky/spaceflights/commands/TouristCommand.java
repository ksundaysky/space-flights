package com.ksundaysky.spaceflights.commands;



import java.util.ArrayList;
import java.util.List;

//@Data
public class TouristCommand
{

    private Long id;

    private String name;
    private String surname;
    private String sex;
    private String country;
    private String dateOfBirth;
    private List<GetFlight> flights = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<GetFlight> getFlights() {
        return flights;
    }

    public void setFlights(List<GetFlight> flights) {
        this.flights = flights;
    }
}
