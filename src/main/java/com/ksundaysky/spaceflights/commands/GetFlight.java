package com.ksundaysky.spaceflights.commands;

import com.ksundaysky.spaceflights.domain.Flight;
import lombok.Data;

import java.util.Date;

@Data
public class GetFlight {

    private Long id;
    private Date dateTimeDeparture;
    private Date dateTimeArrival;
    private String whereFrom;
    private String whereTo;

    public GetFlight(Flight flight) {
        this.id = flight.getId();
        this.dateTimeDeparture = flight.getDateAndTimeDeparture();
        this.dateTimeArrival = flight.getDateAndTimeArrival();
        this.whereFrom = flight.getWhereFrom();
        this.whereTo = flight.getWhereTo();
    }
}
