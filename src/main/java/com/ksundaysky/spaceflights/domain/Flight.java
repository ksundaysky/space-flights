package com.ksundaysky.spaceflights.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date dateAndTimeDeparture;
    private Date dateAndTimeArrival;
    private  String whereFrom;
    private  String whereTo;
    private Long places;

    @ManyToMany(mappedBy = "flights")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private List<Tourist> tourists;

    private Double price;
}
