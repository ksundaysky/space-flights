package com.ksundaysky.spaceflights.bootstrap;

import com.ksundaysky.spaceflights.domain.Flight;
import com.ksundaysky.spaceflights.domain.Tourist;
import com.ksundaysky.spaceflights.repositories.FlightRepository;
import com.ksundaysky.spaceflights.repositories.TouristRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

@Component
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {


    private TouristRepository touristRepository;

    private FlightRepository flightRepository;

    public Bootstrap(TouristRepository touristRepository, FlightRepository flightRepository) {
        this.touristRepository = touristRepository;
        this.flightRepository = flightRepository;
    }


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        try {
            prepareData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

//    @Transactional
    private void prepareData() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdft = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Tourist johnDoe = new Tourist();
        johnDoe.setName("John");
        johnDoe.setSurname("Doe");
        johnDoe.setSex("male");
        johnDoe.setCountry("USA");
        johnDoe.setDateOfBirth(sdf.parse("1996-03-03"));
        Tourist fooBar = new Tourist();
        fooBar.setName("Foo");
        fooBar.setSurname("Bar");
        fooBar.setSex("female");
        fooBar.setCountry("Australia");
        fooBar.setDateOfBirth(sdf.parse("1996-04-04"));
        Flight earthToMars = new Flight();
        earthToMars.setWhereFrom("Earth");
        earthToMars.setWhereTo("Mars");
        earthToMars.setPlaces(5L);
        earthToMars.setPrice(2560000d);
        earthToMars.setDateAndTimeDeparture(sdft.parse("2045-04-04 12:03"));
        earthToMars.setDateAndTimeArrival(sdft.parse("2065-05-12 13:12"));
        Flight earthToVenus = new Flight();
        earthToVenus.setWhereFrom("Earth");
        earthToVenus.setWhereTo("Venus");
        earthToVenus.setPlaces(3L);
        earthToVenus.setPrice(5120000d);
        earthToVenus.setDateAndTimeDeparture(sdft.parse("2055-03-02 11:53"));
        earthToVenus.setDateAndTimeArrival(sdft.parse("2086-02-23 01:32"));
        Flight earthToSaturn = new Flight();
        earthToSaturn.setWhereFrom("Earth");
        earthToSaturn.setWhereTo("Saturn");
        earthToSaturn.setPlaces(7L);
        earthToSaturn.setPrice(100000d);
        earthToSaturn.setDateAndTimeDeparture(sdft.parse("2086-09-11 21:02"));
        earthToSaturn.setDateAndTimeArrival(sdft.parse("2125-05-15 14:13"));
        johnDoe.setFlights(Arrays.asList(earthToMars,earthToVenus));
        fooBar.setFlights(Arrays.asList(earthToMars,earthToSaturn));

        flightRepository.saveAll(Arrays.asList(earthToSaturn,earthToMars,earthToVenus));

        touristRepository.saveAll(Arrays.asList(fooBar,johnDoe));

    }
}
