package com.ksundaysky.spaceflights.repositories;

import com.ksundaysky.spaceflights.domain.Flight;
import org.springframework.data.repository.CrudRepository;

public interface FlightRepository extends CrudRepository<Flight,Long> {
}
