package com.ksundaysky.spaceflights.repositories;

import com.ksundaysky.spaceflights.domain.Flight;
import com.ksundaysky.spaceflights.domain.Tourist;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TouristRepository extends CrudRepository<Tourist,Long>{

    List<Flight> findFlightsById(Long id);
}
