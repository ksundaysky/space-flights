# space-flights ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

space-flights is a Java Web Application to manage space flights.

## Installation

### Requirements
* Window/Linux
* Java 8 and up
* Web browser: Chrome 69.0.3497 (recomended)
* Maven 3.5.0 and up

### Sturtup from console
`mvn spring-boot:run`

Aplication starts on port: 8080

[Link to application](localhost:8080) 


### Technology stack
* Java 8
* Spring Boot 2.0.5
* Tomcat
* H2 Database
* Hibernate 
* Lombok
* HTML
* CSS
* Bootstrap
* JavaScript

## Possibilities

#### Starting page:
![Starting page](https://i.imgur.com/ZK8mugO.png)


---


#### You can go to tourist list:
![Starting page](https://i.imgur.com/bJCXLKy.png)


---


#### Tourist List:
![Tourist List](https://i.imgur.com/oVgzGsr.png)


---



#### Tourist info with list of flights [HERE you can delete tourist or delete flights from this tourist]:
![Tourist Info](https://i.imgur.com/kRCsnJ5.png)


---


#### Starting page, click Add tourist:
![Starting Page](https://i.imgur.com/HQfkeAj.png)


---



#### Tourist create form: 
![Tourist Create Form](https://i.imgur.com/9VdER4w.png)


---



#### Starting page, click flights list:
![Starting Page](https://i.imgur.com/xOFhSLR.png)


---



#### Flight list, you can click flight info:
![Flight List](https://i.imgur.com/j20rLAC.png)


---



#### Flight info with list of tourists [HERE you can delete flight or delete tourists from this flight]:
![Flight Info](https://i.imgur.com/8bdjUoL.png)


---



#### When you click add tourist:
![Flight add tourist](https://i.imgur.com/rWKZpV7.png)


---



#### Flight info with just added tourist:
![Flight Info](https://i.imgur.com/T3LZ3ND.png)


---



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)